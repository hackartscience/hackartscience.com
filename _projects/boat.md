---
title: Little Red Boat
logo: https://static.hackartscience.com/hackartscience/logo/boat.svg
link: http://littleredboat.hackartscience.com/
featured: true
---

Sailboats are often seen as a lavish status symbol, but we believe sailing is within reach of any motivated group or individual.
We started a sailing co-op of our own with our adorable little sailboat, "Leah's Heritage", a 26-foot semi-custom [O'Day Outlaw][outlaw] (designed by [Philip Rhodes][rhodes], a capable heavy displacement cruising sloop.
We take care to detail our maintenance and improvement projects, our trips, and our expenses as a free and open resource to DIY sailors everywhere.
We undertook an extensive rebuild of the ship's rudder and bulkheads, refitting the mast, repainted the hull, refitted seacocks, and carefully documented our work.
Read about our adventures, and come sailing with us!

[outlaw]: http://sailboatdata.com/viewrecord.asp?class_id=436
[rhodes]: https://en.wikipedia.org/wiki/Philip_Rhodes
