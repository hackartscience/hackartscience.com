---
title: Files
logo: https://static.hackartscience.com/hackartscience/logo/files.svg
link: https://files.hackartscience.com
---

We host a private cloud (powered by [ownCloud][owncloud]), and use it to manage the web-facing resources used by all of our websites.
This makes our [GitHub Pages][githubpages] cost-free hosting solution a very capable one indeed: resources that do not belong in a repository are managed by this very convenient CMS.
It's also our own private encrypted [Dropbox][dropbox]!

[owncloud]: https://owncloud.org/
[githubpages]: https://pages.github.com/
[dropbox]: https://www.dropbox.com/
