---
title: Laundromat
logo: https://static.hackartscience.com/hackartscience/logo/laundromat.svg
subtext: (retired)
featured: true
---

We at hackartscience love film and TV, but are not very fond of carrying our obscure collections around with us.
We do, however, usually have access to the internet.
We curated our movie collection, and hosted the resulting rare media wonderland &agrave; la [Netflix][netflix] via a web application with some strong security guarantees.
Laundromat is hosted on several collaborating tiny [Raspberry Pi][raspberrypi] devices, and is capable of streaming high-bandwidth video to a handful of users concurrently.

[netflix]: http://netflix.com/
[raspberrypi]: https://www.raspberrypi.org/
