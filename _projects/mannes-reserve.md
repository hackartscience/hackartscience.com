---
title: Mannes-Reserve
logo: https://static.hackartscience.com/hackartscience/logo/mannes-reserve.svg
subtext: (retired)
---

Reserving rooms at [Mannes New School][mannes] is annoying, especially if you are hoping to establish a regular schedule.
Do you want this process automated?
This little app does just that: given a weekly schedule, it reserves your preferred rooms in a timely manner.
Let us know if you would like your practice room reservations automated!


[mannes]: http://www.newschool.edu/mannes/
