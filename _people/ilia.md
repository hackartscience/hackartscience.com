---
name: Ilia
avatar: https://static.hackartscience.com/hackartscience/people/ilia.svg
link: http://ilia.hackartscience.com
order: 1
---

Ilia is a PhD student in computer systems security at MIT's Computer Science and Artificial Intelligence Laboratory.
He has a passion for open education, and holds his work to a high standard with respect to polish and accessibility.
Outside work, Ilia is a sailing vessel captain, an opportunistic bartender, and an all-around inquisitive individual.
