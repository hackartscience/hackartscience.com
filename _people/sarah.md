---
name: Sarah
avatar: https://static.hackartscience.com/hackartscience/people/sarah.svg
link: http://sarahycheng.com
order: 2
---

Sarah is an MIT alumni and a full-stack engineer working on all things web-related.
Sarah is a philosophy and video game enthusiast, and spends a lot of time thinking of ways to effect maximum good.
